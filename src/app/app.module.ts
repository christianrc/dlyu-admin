import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductsComponent } from './pages/maintenance/products/products.component';
import { UsersComponent } from './pages/maintenance/users/users.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductsEditionComponent } from './pages/maintenance/products/products-edition/products-edition.component';
import { TypeProductsComponent } from './pages/maintenance/type-products/type-products.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { UsersEditionComponent } from './pages/maintenance/users/users-edition/users-edition.component';
 
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    UsersComponent,
    LoginComponent,
    ProductsEditionComponent,
    TypeProductsComponent,
    UsersEditionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
