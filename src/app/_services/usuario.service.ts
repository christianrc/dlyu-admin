import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = `${HOST}/api/User`;

  constructor(private readonly http : HttpClient) { }

  getUsuarioById(headers, params) {
    return this.http.get(`${this.url}/GetUsuarioPorId?id=`+  params ,{headers});
  }
  
  
  getUsuario(headers) {
    return this.http.get(`${this.url}/getusers`, {headers});
  }

  getUsuarioByCriterio(data, headers) {
    console.log(data);
    return this.http.post(`${this.url}/getuser`, data  ,{headers});
  }
  
  insert(data, headers) {
    data.iD_USUARIO= +data.iD_USUARIO;
    data.iD_TIPO_DOCUMENTO = +data.iD_TIPO_DOCUMENTO;
    data.iD_DISTRITO = +data.iD_DISTRITO;
    data.iD_TIPO_USUARIO = 2;
    console.log(data);
    return this.http.post<any>(`${this.url}/insert`, data , { headers});
  }

  edit(data, headers) {
    data.iD_USUARIO= +data.iD_USUARIO;
    data.iD_TIPO_DOCUMENTO = +data.iD_TIPO_DOCUMENTO;
    data.iD_DISTRITO = +data.iD_DISTRITO;
    return this.http.post<any>(`${this.url}/edit`, data , { headers});
  }

  
  delete(params, headers) {
    return this.http.post<any>(`${this.url}/delete?id=`+  params  , { headers});
  }

}
