import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class DistritoService {
 
  url: string = `${HOST}/api/Distrito`;

  constructor(private readonly http : HttpClient) { }

  getDistrito(headers) {
    return this.http.get(`${this.url}/getdistritos`, {headers});
  }
}
