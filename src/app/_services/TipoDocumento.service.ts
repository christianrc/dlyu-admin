import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumento {
 
  url: string = `${HOST}/api/TipoDocumento`;

  constructor(private readonly http : HttpClient) { }

  getTipoDocumento(headers) {
    return this.http.get(`${this.url}/getTipoDocumentos`, {headers});
  }
}
