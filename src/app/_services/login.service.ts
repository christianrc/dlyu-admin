import { Injectable } from '@angular/core';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  url: string = `${HOST}/api/User`;

  constructor(private readonly http : HttpClient) { }

  login(data) {
    console.log(data);
    return this.http.post<any>(`${this.url}/login`, data);
  }

  estaLogeado(){
    let token = sessionStorage.getItem(TOKEN_NAME);
    //return true;
    return token != null;
  }

}
