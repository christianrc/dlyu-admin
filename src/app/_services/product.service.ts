import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url: string = `${HOST}/api/Producto`;

  constructor(private readonly http : HttpClient) { }

  getProductsById(headers, params) {
    return this.http.get(`${this.url}/getproductobyid?id=`+  params ,{headers});
  }
  
  
  getProducts(headers) {
    return this.http.get(`${this.url}/getproductos`, {headers});
  }

  getProductsByCriterio(data, headers) {
    console.log(data);
    return this.http.post(`${this.url}/getproductobycriterion`, data  ,{headers});
  }
  
  insert(data, headers) {
    data.iD_PRODUCTO= +data.iD_PRODUCTO;
    data.iD_TIPO_PRODUCTO = +data.iD_TIPO_PRODUCTO;
    return this.http.post<any>(`${this.url}/insert`, data , { headers});
  }

  edit(data, headers) {
    data.iD_PRODUCTO= +data.iD_PRODUCTO;
    data.iD_TIPO_PRODUCTO = +data.iD_TIPO_PRODUCTO;
    return this.http.post<any>(`${this.url}/edit`, data , { headers});
  }

  
  delete(params, headers) {
    return this.http.post<any>(`${this.url}/delete?id=`+  params  , { headers});
  }

}
