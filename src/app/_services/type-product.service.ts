import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class TypeProductService {
 
  url: string = `${HOST}/api/TipoProducto`;

  constructor(private readonly http : HttpClient) { }

  getTypeProducts(headers) {
    return this.http.get(`${this.url}/gettipoproductos`, {headers});
  }
}
