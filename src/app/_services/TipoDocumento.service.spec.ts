import { TestBed } from '@angular/core/testing';
import { TipoDocumento } from './TipoDocumento.service';

describe('DistritoService', () => {
  let service: TipoDocumento;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoDocumento);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
