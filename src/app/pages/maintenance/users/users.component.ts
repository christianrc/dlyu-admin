import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DistritoService } from 'src/app/_services/distrito.service';
import { TipoDocumento } from 'src/app/_services/TipoDocumento.service';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  usuarios = [];


  UsuariosForm = this.fb.group({
    numerO_DOCUMENTO: '',
    nombre: '',
    apellidO_PATERNO: ''    
  })
  constructor(private fb: FormBuilder,private readonly distrtoService: DistritoService,private readonly tipoDocumento: TipoDocumento, private readonly usuarioService: UsuarioService,  private router: Router) { }

  ngOnInit(): void {
    this.getUsuarioByCriterio();
  }


  getUsuarioByCriterio() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.usuarioService.getUsuarioByCriterio(this.UsuariosForm.value, header).subscribe((rest: any) => {
     // this.project = rest.data.filter((item: { id: number }) => item.id == id);
      console.log(rest);

      if(rest.data==null){
        this.usuarios = [];
      }else{
        this.usuarios = rest.data;
      }
    })

  }

  delete_usuario(idUsuario){
    
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};

    this.usuarioService.delete(idUsuario, header).subscribe((rest: any) => {
      console.log(idUsuario);
      console.log(rest);
      
      if(rest.data!=null && rest.data.codigo==0){ 
        this.getUsuarioByCriterio();
      }else{ 
        alert('No se logró eliminar el registro');
      }

    })

  }


}
