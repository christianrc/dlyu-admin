import { Component, OnInit } from '@angular/core';
import { DistritoService } from 'src/app/_services/distrito.service';
import { TipoDocumento } from 'src/app/_services/TipoDocumento.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UsuarioService } from 'src/app/_services/usuario.service';

@Component({
  selector: 'app-users-edition',
  templateUrl: './users-edition.component.html',
  styleUrls: ['./users-edition.component.css']
})
export class UsersEditionComponent implements OnInit {

  distrito = [];
  tipodocumento = [];
  id:number;
  edicion: boolean = false;
  UsuarioForm: FormGroup;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private router: Router,private readonly distrtoService: DistritoService,private readonly tipoDocumento: TipoDocumento,private readonly usuarioService: UsuarioService) { 
    this.UsuarioForm = this.fb.group({
      iD_USUARIO: 0,
      nombre: ['', Validators.required],
      apellidO_PATERNO: ['', Validators.required],
      apellidO_MATERNO: ['', Validators.required],
      celular: ['', Validators.required],
      direccion: ['', Validators.required],      
      iD_DISTRITO: [0, Validators.required],
      iD_TIPO_DOCUMENTO: [0, Validators.required],
      numerO_DOCUMENTO: ['', Validators.required],
      iD_TIPO_USUARIO: 2,
      correo: ['', Validators.required],
      contrasena: ['', Validators.required],
      genero: ['', Validators.required]
    });
  }


  ngOnInit(): void {
    this.getDistrito();
    this.getTipoDocumento();
    this.route.params.subscribe((params: Params) => {
      this.id = params ['id'];
      this.edicion = params ['id'] != null;
      this.initForm();
      
    })
  }

  getDistrito() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.distrtoService.getDistrito(header).subscribe((rest: any) => {
      if(rest.data==null){
        this.distrito = [];
      }else{
        console.log(rest);
        this.distrito = rest.data;
      }
    })
  }

  getTipoDocumento() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.tipoDocumento.getTipoDocumento(header).subscribe((rest: any) => {
      if(rest.data==null){
        this.tipodocumento = [];
      }else{
        console.log(rest);
        this.tipodocumento = rest.data;
      }
    })
  }

  
  initForm(){
    if(this.edicion){
      const token = sessionStorage.getItem(TOKEN_NAME);
      const header = { Authorization: 'Bearer '+ token}; 

      this.usuarioService.getUsuarioById(header, this.id).subscribe((rest: any) =>{

        this.UsuarioForm = this.fb.group({
          iD_USUARIO: rest.data.iD_USUARIO,
          nombre: rest.data.nombre,
          apellidO_PATERNO: rest.data.apellidO_PATERNO,
          apellidO_MATERNO: rest.data.apellidO_MATERNO,
          celular: rest.data.celular,
          direccion: rest.data.direccion,
          iD_DISTRITO: rest.data.iD_DISTRITO,
          iD_TIPO_DOCUMENTO: rest.data.iD_TIPO_DOCUMENTO,
          numerO_DOCUMENTO: rest.data.numerO_DOCUMENTO,
          iD_TIPO_USUARIO: rest.data.iD_TIPO_USUARIO,
          correo: rest.data.correo,
          contrasena: rest.data.contrasena,
          genero: rest.data.genero
        });

        const fgNOMBRE: HTMLElement = document.getElementById('fg-NOMBRE');
        const fgAPELLIDO_PATERNO: HTMLElement = document.getElementById('fg-APELLIDO_PATERNO');
        const fgAPELLIDO_MATERNO: HTMLElement = document.getElementById('fg-APELLIDO_MATERNO');
        const fgCELULAR: HTMLElement = document.getElementById('fg-CELULAR');
        const fgDIRECCION: HTMLElement = document.getElementById('fg-DIRECCION');
        const fgiD_DISTRITO: HTMLElement = document.getElementById('fg-iD_DISTRITO');
        const fgiD_TIPO_DOCUMENTO: HTMLElement = document.getElementById('fg-iD_TIPO_DOCUMENTO');
        const fgNUMERO_DOCUMENTO: HTMLElement = document.getElementById('fg-NUMERO_DOCUMENTO');
        const fgID_TIPO_USUARIO: HTMLElement = document.getElementById('fg-ID_TIPO_USUARIO');
        const fgCORREO: HTMLElement = document.getElementById('fg-CORREO');
        const fgCONTRASENA: HTMLElement = document.getElementById('fg-CONTRASENA');
        const fgGENERO: HTMLElement = document.getElementById('fg-GENERO');

        fgNOMBRE.className += " is-filled";
        fgAPELLIDO_PATERNO.className += " is-filled";
        fgAPELLIDO_MATERNO.className += " is-filled";
        fgCELULAR.className += " is-filled";
        fgDIRECCION.className += " is-filled";
        fgNUMERO_DOCUMENTO.className += " is-filled";
        fgCORREO.className += " is-filled";
        fgCONTRASENA.className += " is-filled";
        fgGENERO.className += " is-filled";

      });

    }
  }

  ngAfterViewInit() {

  }

  insert(data) {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token}; 
    
    this.usuarioService.insert(data,header).subscribe((rest: any) => {
      if(rest.isSuccess) {
        this.router.navigate(['usuarios']);
      } else {
        alert(rest.errorMessage);
      }
    })

  }

  edit(data) {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token}; 
    
    this.usuarioService.edit(data,header).subscribe((rest: any) => {
      if(rest.isSuccess) {
        this.router.navigate(['usuarios']);
      } else {
        alert(rest.errorMessage);
      }
    })
  }

  onSubmit() {
    console.log(this.UsuarioForm.value);
    if(this.UsuarioForm.valid) {
        if(this.edicion){
          this.edit(this.UsuarioForm.value);
        }else {
          this.insert(this.UsuarioForm.value);
        }      
    }
  }
}
