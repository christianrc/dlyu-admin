import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsEditionComponent } from './products-edition.component';

describe('ProductsEditionComponent', () => {
  let component: ProductsEditionComponent;
  let fixture: ComponentFixture<ProductsEditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsEditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
