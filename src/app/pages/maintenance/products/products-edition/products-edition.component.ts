import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductService } from 'src/app/_services/product.service';
import { TypeProductService } from 'src/app/_services/type-product.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';

@Component({
  selector: 'app-products-edition',
  templateUrl: './products-edition.component.html',
  styleUrls: ['./products-edition.component.css']
})
export class ProductsEditionComponent implements OnInit {

  typeProducts = [];
  id:number;
  edicion: boolean = false;
  productForm: FormGroup;

  element: HTMLElement;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private router: Router, 
    private readonly typeProductService: TypeProductService, private readonly productService : ProductService) {

    this.productForm = this.fb.group({
      iD_PRODUCTO: '0',
      nombre: ['', Validators.required],
      descripcion: '',
      stock: '',
      precio: ['', Validators.required],
      preciO_SUGERIDO: '',
      sabor: '',
      iD_TIPO_PRODUCTO: ['0', Validators.required]
    });
   }

  ngOnInit(): void {
    this.getTypeProduct();
    this.route.params.subscribe((params: Params) => {
      this.id = params ['id'];
      this.edicion = params ['id'] != null;
      this.initForm();
      
    })
  }

  getTypeProduct() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.typeProductService.getTypeProducts(header).subscribe((rest: any) => {
      if(rest.data==null){
        this.typeProducts = [];
      }else{
        this.typeProducts = rest.data;
      }
    })
  }
 
  initForm(){
    if(this.edicion){
      const token = sessionStorage.getItem(TOKEN_NAME);
      const header = { Authorization: 'Bearer '+ token}; 

      this.productService.getProductsById(header, this.id).subscribe((rest: any) =>{

        this.productForm = this.fb.group({
          iD_PRODUCTO: rest.data.iD_PRODUCTO,
          nombre: rest.data.nombre,
          descripcion: rest.data.descripcion,
          stock: rest.data.stock,
          precio: rest.data.precio,
          preciO_SUGERIDO: rest.data.preciO_SUGERIDO,
          sabor: rest.data.sabor,
          iD_TIPO_PRODUCTO: rest.data.iD_TIPO_PRODUCTO
        });

        const fgNombre: HTMLElement = document.getElementById('fg-nombre');
        const fgDescripcion: HTMLElement = document.getElementById('fg-descripcion');
        const fgStock: HTMLElement = document.getElementById('fg-stock');
        const fgPrecio: HTMLElement = document.getElementById('fg-precio');
        const fgPrecioSugerido: HTMLElement = document.getElementById('fg-precio-sugerido');
        const fgSabor: HTMLElement = document.getElementById('fg-sabor');

        fgNombre.className += " is-filled";
        fgDescripcion.className += " is-filled";
        fgStock.className += " is-filled";
        fgPrecio.className += " is-filled";
        fgPrecioSugerido.className += " is-filled";
        fgSabor.className += " is-filled";

      });

    }
  }

  ngAfterViewInit() {
    const fgNombre: HTMLElement = document.getElementById('fg-nombre'); 
    fgNombre.className += " is-filled";
  }

  insert(data) {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token}; 
    
    this.productService.insert(data,header).subscribe((rest: any) => {
      if(rest.isSuccess) {
        this.router.navigate(['productos']);
      } else {
        alert(rest.errorMessage);
      }
    })

  }

  edit(data) {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token}; 
    
    this.productService.edit(data,header).subscribe((rest: any) => {
      if(rest.isSuccess) {
        this.router.navigate(['productos']);
      } else {
        alert(rest.errorMessage);
      }
    })

  }

  onSubmit() {
    console.log(this.productForm.value);
    if(this.productForm.valid) {
      let tipoProducto = +this.productForm.value.iD_TIPO_PRODUCTO;
      if(tipoProducto > 0) {
        if(this.edicion){
          this.edit(this.productForm.value);
        }else {
          this.insert(this.productForm.value);
        }
      } else{
        alert('Seleccione una opción de tipo de producto');
      }
    }
  }

}