import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/_services/product.service';
import { TypeProductService } from 'src/app/_services/type-product.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
 
  products = [];
  typeProducts = [];

  productForm = this.fb.group({
    idTipoProducto: '0',
    nombre: '',
    sabor: '',
  });
  
  constructor(private fb: FormBuilder,private readonly productService: ProductService,
    private readonly typeProductService: TypeProductService,  private router: Router) { }
 
  ngOnInit(): void {
    this.getTypeProduct();
    this.getProductByCriterio();
  }

  getTypeProduct() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.typeProductService.getTypeProducts(header).subscribe((rest: any) => {
      if(rest.data==null){
        this.typeProducts = [];
      }else{
        console.log('aaaaaaaa');
        console.log(rest);
        this.typeProducts = rest.data;
      }
    })
  }
  
  getProductByCriterio() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};
    
    this.productService.getProductsByCriterio(this.productForm.value, header).subscribe((rest: any) => {
     // this.project = rest.data.filter((item: { id: number }) => item.id == id);
      console.log(rest);

      if(rest.data==null){
        this.products = [];
      }else{
        this.products = rest.data;
      }
    })

  }

  delete_product(idProducto){
    
    const token = sessionStorage.getItem(TOKEN_NAME);
    const header = { Authorization: 'Bearer '+ token};

    this.productService.delete(idProducto, header).subscribe((rest: any) => {
      console.log(idProducto);
      console.log(rest);
      
      if(rest.data!=null && rest.data.codigo==0){ 
        this.getProductByCriterio();
      }else{ 
        alert('No se logró eliminar el registro');
      }

    })

  }

}