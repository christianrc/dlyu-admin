import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/_services/login.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm = this.fb.group({
    loginUsuario: ['', Validators.required],
    passwordUsuario: ['', Validators.required]
  });

  constructor(private fb: FormBuilder, private readonly loginService : LoginService,  private router : Router) { }

  login(dataUser) {

    this.loginService.login(dataUser).subscribe((rest : any) => {
      if(rest.isSuccess) {
        sessionStorage.setItem(TOKEN_NAME, rest.data.token);
        console.log(sessionStorage.getItem(TOKEN_NAME));
        //alert("Login Exitoso");
        this.router.navigate(['inicio']);
      } else {
        alert("Credenciales Invalidas");
      }
    })
    
  }

  onSubmit() {
    if(this.loginForm.valid) {
      this.login(this.loginForm.value);
    } 
    else {
      alert("Formulario no valido");
    }
  }

  ngOnInit(): void {
    
  }

}