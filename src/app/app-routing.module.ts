import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductsEditionComponent } from './pages/maintenance/products/products-edition/products-edition.component';
 
import { ProductsComponent } from './pages/maintenance/products/products.component';
import { TypeProductsComponent } from './pages/maintenance/type-products/type-products.component';
import { UsersEditionComponent } from './pages/maintenance/users/users-edition/users-edition.component';
import { UsersComponent } from './pages/maintenance/users/users.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent }, 
  { path: 'inicio', component: HomeComponent }, 
  { path: 'tipo-productos', component: TypeProductsComponent},
  { path: 'productos', component: ProductsComponent},
  { path: 'productos-nuevo', component: ProductsEditionComponent },
  { path: 'usuario-nuevo', component: UsersEditionComponent },
  { path: 'productos-edicion/:id', component: ProductsEditionComponent },
  { path: 'usuario-edicion/:id', component: UsersEditionComponent },
  { path: 'usuarios', component: UsersComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
